﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using todoApp.Models;

namespace todoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public static List<User> UserStore = new List<User>();
        public static List<User> AdminStore = new List<User>();
        private readonly DataContext _dbContext;
       

        public UsersController(DataContext dbContext)
        {
            _dbContext = dbContext;
        }


        [HttpGet]
        public List<User> GetUser()
        {
            return _dbContext.Users.ToList();
        }

        [HttpPost("userpage/{page}")]
        public List<User> userByPageNo([FromRoute] int page)
        {
            //int totalRecords = _dbContext.Users.Count();


            List<User> userbypage = _dbContext.Users.Skip((page - 1) * 5).Take(5).OrderBy(x => x.email).ToList();



            return userbypage;

        }


        // Jwt Token Generation

        [HttpPost("gettoken")]
        public IActionResult GetToken([FromBody] User user)
        {
            string userEmail = "";
            string userPassword = "";
            string Roles = "notfound";
            string key = "my_secret_key_12345";
            var issuer = "umer.com";
            var audience = "ali.com";
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            User AdminObj = new User();
            AdminObj.email = "admin@gmail.com";
            AdminObj.password = "Admin@123";
            AdminStore.Add(AdminObj);
            
            //CheckToFind User Or Admin

            for (int i = 0; i < AdminStore.Count; i++)
            {
                if (AdminStore[i].email == user.email && AdminStore[i].password == user.password)
                {
                    userEmail = AdminStore[i].email;
                    userPassword = AdminStore[i].password;
                    Roles = "Admin";
                }


            }
            if (Roles != "Admin")
            {
                var data = _dbContext.Users.Find(user.email);
                if (data != null)
                {
                    if (data.email == user.email && data.password == user.password)
                    {
                        Roles = "User";
                    }
                }
                
            }
            
            if (Roles == "notfound") {
                return Conflict();
            }
            

            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("valid", "1"));
            permClaims.Add(new Claim("userid", "1"));
            permClaims.Add(new Claim("email", userEmail));
            permClaims.Add(new Claim("pass", userPassword));
            permClaims.Add(new Claim("Roles", Roles));
            var token = new JwtSecurityToken(issuer,
                            audience,
                            permClaims,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(new { data = jwt_token });
        }

        
        [HttpGet("validatetoken")]
        public IActionResult UserAuthent()
        {
            if (User.Identity.IsAuthenticated)
            {
                var claim = User.Claims.First(c => c.Type == "Roles");
                string role = claim.Value;

                return Ok(new { Role = role });
            }
            else
                return NotFound();

        }


        [HttpPost]

        public IActionResult CreateUser([FromBody] User user)
        {
            user.email = user.email;
            user.password = user.password;

            User data = _dbContext.Users.Find(user.email);
            
            if (data!=null)
            {
                return Conflict(new { message = "User already exists" });
            }
            else
            {
                _dbContext.Users.Add(user);
                _dbContext.SaveChanges();
                return Ok(new { message = "User Added Successfuly" });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        

        [HttpPut("UserEmailVerification")]
        public bool GetUser([FromBody] User user)
        {

            var data = _dbContext.Users.Find(user.email);
            if (data.email== user.email && data.password== user.password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

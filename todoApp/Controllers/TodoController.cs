﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using todoApp.Models;

namespace todoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        
        public static List<Todo> TodosStore = new List<Todo>();
        private readonly DataContext _dbcontext;
        public TodoController(DataContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public Todo Create([FromBody] Todo todo)
        {
            _dbcontext.Todos.Add(todo);
            _dbcontext.SaveChanges();
            return todo;
        }


        /*
        //[Authorize(Roles = "Admin,User")]
        [HttpGet]
        public List<Todo> GetAll()
        {
            return _dbcontext.Todos.ToList();
        }*/
        [HttpGet("tpages")]
        public int getTotalPages()
        {
            var TotalPages = _dbcontext.Todos.Count() / 5;
            
            return TotalPages;
        }

        [HttpGet("stodos")]
        public List<Todo> GetAllSortedTodos([FromQuery] int pg, [FromQuery] string sort)
        {
            var userbypage = _dbcontext.Todos.Skip((pg - 1) * 6).Take(6);
            switch (sort)
            {
                case "username":
                    userbypage = userbypage.OrderBy(p => p.UserName);
                    break;
                case "todoname":
                    userbypage = userbypage.OrderBy(p => p.TodoName);
                    break;
                case "iscompleted":
                    userbypage = userbypage.OrderBy(p => p.IsComplete);
                    break;
            }
            return userbypage.ToList();
        }

        [Authorize(Roles = "User")]
        [HttpPut("todosbyuser")]
        public List<Todo> GetTodosByUser([FromBody] Todo todo)
        {
            List<Todo> UserTodoStore = new List<Todo>();
            
            var TodosFromDatabase = _dbcontext.Todos.ToList();

            for (int i = 0; i < TodosFromDatabase.Count; i++)
            {

                if (TodosFromDatabase[i].UserName == todo.UserName)
                {
                    UserTodoStore.Add(TodosFromDatabase[i]);

                }
            }
                return UserTodoStore;
        }


        [Authorize(Roles = "User")]
        [HttpDelete("{id}")]
        public Todo DeleteItem([FromRoute] int id)
        {

            Todo DelKey = _dbcontext.Todos.Find(id);
            _dbcontext.Todos.Remove(DelKey);
            _dbcontext.SaveChanges();
            return DelKey;
        }



        [Authorize(Roles = "User")]
        [HttpPut("{id}")]
        public IActionResult update([FromRoute] int id, [FromBody] Todo todo) {

            Todo UpdateKey = _dbcontext.Todos.Find(id);
            _dbcontext.Todos.Update(UpdateKey);
            _dbcontext.SaveChanges();
            


            var result = _dbcontext.Todos.SingleOrDefault(b => b.Id == id);
            if (result != null)
            {
                result.TodoName = todo.TodoName;
                _dbcontext.SaveChanges();
            }
            return Ok();


        }




        [Authorize(Roles = "User")]
        [HttpPut("updateiscomplete/{id}")]
        public IActionResult UpdateisComplete([FromRoute] int id, [FromBody] Todo todo)
        {
            Todo UpdateKey = _dbcontext.Todos.Find(id);
            var result = _dbcontext.Todos.SingleOrDefault(res => res.Id == id);
             if (result!=null)
            {
                result.IsComplete = todo.IsComplete;
                _dbcontext.SaveChanges();
            }
            return Ok();
        }


    }

}

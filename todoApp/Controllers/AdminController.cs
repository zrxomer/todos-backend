﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todoApp.Models;

namespace todoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Controller
    {
        private readonly DataContext _dbcontext;
        public AdminController(DataContext dbContext)
        {
            _dbcontext = dbContext;
        }

        [Authorize(Roles ="Admin")]
        [HttpGet("AllTodos")]
        public List<Todo> GetAllTodos()
        {
            return _dbcontext.Todos.ToList();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public List<User> GetUser()
        {
            return _dbcontext.Users.ToList();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult Create([FromBody] User user)
        {
            var existingUser = _dbcontext.Users.Count(a => a.email == user.email);
            if (existingUser == 0)
            {
                _dbcontext.Users.Add(user);
                _dbcontext.SaveChanges();
                return Ok("User Successfuly Saved..");
            }
            else
            {
                return Conflict();
            }

        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{email}")]
        public IActionResult DeleteItem([FromRoute] string email)
        {
            
            
            var DelK = _dbcontext.Users.FirstOrDefault(res => res.email ==email);
            if(DelK!=null){
                _dbcontext.Users.Remove(DelK);
                _dbcontext.SaveChanges();
                return Ok("User Deleted Successfuly");
            }
            else
            {
                return Conflict("User does not exists");
            }
            


        }
        [Authorize(Roles = "Admin")]
        [HttpPut("{email}")]
        public IActionResult update([FromRoute] string email, [FromBody] User user)
        {

            var result = _dbcontext.Users.SingleOrDefault(b => b.email == email);
            if (result != null)
            {
                result.password = user.password;
                _dbcontext.SaveChanges();
            }
            return Ok();

        }

    }
}

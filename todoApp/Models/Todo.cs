﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace todoApp.Models
{
    public class Todo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(25, ErrorMessage = "The Length of {0} is more than 25")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "The name of {0} is Required")]
        [StringLength(25, ErrorMessage = "The Length of {0} is more than 25")]
        public string TodoName { get; set; }

        [Required(ErrorMessage = "The name of {0} is Required")]
        public bool IsComplete { get; set; }
    }
}

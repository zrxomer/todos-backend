﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace todoApp.Models
{
    public class User
    {
        [Required(ErrorMessage ="The name of {0} is Required")]
        [StringLength(25, ErrorMessage = "The Length of {0} is more than 25")]
        [EmailAddress(ErrorMessage = "The {0} is not valid")]
        [Key]
        public string email { get; set; }
        
        
        [Required(ErrorMessage = "The name of {0} is Required")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{6,20}$", ErrorMessage = "Invalid Password: Password must be min of length 6 and max of 20 with atleast Numeric number.")]
        public string password { get; set; }


    }
}
